import { TrainingUser } from './training-user';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface Organizer extends TrainingUser {}
