# KYPO Training Model

This library contains frontend model of [KYPO Training service](https://gitlab.ics.muni.cz/kypo-crp/backend-java/kypo2-training).

## Prerequisites
To use the library you need to have installed:
* NPM with access to [KYPO registry](https://projects.ics.muni.cz/projects/kbase/knowledgebase/articles/153)

## Usage
To use the model in your project follow these steps:
1. Run `npm install @muni-kypo-crp/training-model`
2. Import classes like usual, for example `import { TrainingDefinition } from '@muni-kypo-crp/training-model`
